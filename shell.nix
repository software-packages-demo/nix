{ pkgs ? import <nixpkgs> {}
}:
pkgs.mkShell {
    name="dev-environment";
    buildInputs = [
        pkgs.busybox
    ];
    shellHook = ''
        echo "Start developing..."
    '';
}
