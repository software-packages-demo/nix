with import <nixpkgs> {system = "x86_64-linux";};
let
    /* haskell = haskellPackages.ghcWithPackages (pkgs: [
        pkgs.hasql
    ]); # Makes a huge image
    */
in dockerTools.streamLayeredImage {
    name = "busybox-nix-stream";
    tag = "latest";
    created = "now";
    contents = [
        # haskell

        pkgs.busybox
        # pkgs.cabal-install # Keep image little.
    ];
    config = {};
}

/*
{ pkgs ? import <nixpkgs> { system = "x86_64-linux";}
}:
pkgs.dockerTools.streamLayeredImage {
    name = "busybox-nix-stream";
    tag = "latest";
    created = "now";
    contents = [
        pkgs.busybox
        # pkgs.cabal-install
        # pkgs.haskellPackages.ghcWithPackages (pkgs: [pkgs.hasql]) # Does not work.
    ];
    config = {};
}
*/
