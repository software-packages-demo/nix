.variables:
    variables:
        DEBIAN_FRONTEND: noninteractive
        TERM: xterm-color

variables:
    NIX_STABLE: 2.3.16
    NIXOS_STABLE_CHANNEL: nixos-23.05
    NIXOS_OLDSTABLE_CHANNEL: nixos-22.11
    NIXOS_STABLE_URL: https://nixos.org/channels/$NIXOS_STABLE_CHANNEL
    NIXOS_OLDSTABLE_URL: https://nixos.org/channels/$NIXOS_OLDSTABLE_CHANNEL
    NIXPKGS_URL: $NIXOS_STABLE_URL

.busybox-help-sh:
    script:
    - command -v busybox
    - busybox --help sh


###
# nix-shell definitions
###

.nixos-nix-shell-busybox:
    before_script:
    - set -o errexit -o nounset -o noglob -o pipefail
    - shopt -s failglob || true
    - (. /etc/os-release ; echo $PRETTY_NAME) || true
    - command -v nix-shell
    image:
        entrypoint:
        - sh
        - -o
        - xtrace
        - -c
        - nix-shell --run sh --packages busybox 

.nixos-nix-shell-file:
    before_script:
    - set -o errexit -o nounset -o noglob -o pipefail
    - shopt -s failglob || true
    - (. /etc/os-release ; echo $PRETTY_NAME) || true
    - command -v nix-shell
    image:
        entrypoint:
        - sh
        - -o
        - xtrace
        - -c
        - set -o errexit -o nounset -o noglob -o pipefail ;
          shopt -s failglob || true ;
          cd $CI_PROJECT_DIR ;
          nix-shell --run sh

###
# nix-shell
###

nix-shell-install-busybox-nixos:
    extends:
    - .busybox-help-sh
    - .nixos-nix-shell-busybox
    - .variables
    when: manual
    image:
        name: nixos/nix:$NIX_STABLE
    
nix-shell-install-busybox-file-nixos:
    extends:
    - .busybox-help-sh
    - .nixos-nix-shell-file
    - .variables
    when: manual
    image:
        name: nixos/nix:$NIX_STABLE
    
nix-shell-install-busybox-file-debian:
    extends:
    - .busybox-help-sh
    - .nixos-nix-shell-file
    - .variables
    when: manual
    image:
        entrypoint:
        - bash # dash has no time builtin
        - -o
        - xtrace
        - -c
        -   set -o errexit -o nounset -o noglob ;
            cd $CI_PROJECT_DIR ;
            time apt-get update --quiet=2 ;
            time apt-get install
                --no-install-suggests
                --no-install-recommends
                --quiet=2
                    ca-certificates
                    nix
            ;
            nix-channel --list ;
            nix-channel
                --add $NIXPKGS_URL
                    nixpkgs ;
            time nix-channel --update ;
            nix-channel --list ;
            find /nix/store -name nixpkgs ;
            export NIX_PATH="$(
                set +o noglob ;
                cd /nix/store/*-nixpkgs-*/nixpkgs ;
                cd .. ; pwd
                ):$(
                set +o noglob ;
                cd /nix/store/*-user-environment/nixpkgs ;
                cd .. ; pwd)" ;
            nix-shell --run sh
        name: debian:testing

nix-shell-install-busybox-file-archlinux:
    extends:
    - .busybox-help-sh
    - .nixos-nix-shell-file
    - .variables
    when: manual
    image:
        entrypoint:
        - sh
        - -o
        - xtrace
        - -c
        -   set -o errexit -o nounset -o noglob -o pipefail ;
            shopt -s failglob ;
            cd $CI_PROJECT_DIR ;
            time pacman
                --sync
                --refresh
                --noconfirm
                --quiet
                --color always
                    nix
            ;
            nix-channel --list ;
            nix-channel
                --add $NIXPKGS_URL
                    nixpkgs ;
            time nix-channel --update ;
            nix-channel --list ;
            find /nix/store -name nixpkgs ;
            nix-shell --run sh
        name: archlinux

###
# nix-env definitions
###

.nix-env-install-busybox-path: &nix-env-install-busybox-path
- nix-channel --version
- time ${SUDO:-} nix-env --install
    busybox
- $(${SUDO:-} find /nix/store -executable -name busybox -type f) --help sh
- PATH=$PATH:$( set +o noglob ; ls --directory /nix/store/*-busybox-*/bin )

.nix-channel-nixpkgs: &nix-channel-nixpkgs
- ${SUDO:-} nix-channel
    --add $NIXPKGS_URL
        nixpkgs
- ${SUDO:-} nix-channel --update
- ${SUDO:-} nix-channel --list

.nix-path: &nix-path
- find /nix/store -name nixpkgs
- (set +o noglob ; cd /nix/store/*-nixpkgs*/nixpkgs ; cd .. ; pwd)
- (set +o noglob ; cd /nix/store/*-user-environment/nixpkgs ; cd .. ; pwd)
- export NIX_PATH="$(set +o noglob ; cd /nix/store/*-nixpkgs*/nixpkgs ; cd .. ; pwd):$(set +o noglob ; cd /nix/store/*-user-environment/nixpkgs ; cd .. ; pwd)"

.apk: &apk
- set -o errexit -o nounset -o noglob -o pipefail
- (. /etc/os-release ; echo $PRETTY_NAME)
- ${SUDO:-} apk add
        --update-cache
        --quiet
    bash
    bzip2
    coreutils
    curl
    file
    openrc
    perl
    psmisc
    tar
    xz
- ${SUDO:-} apk add
        --quiet
    findutils
    grep
    gzip
    less
    which
    wget
- ${SUDO:-} apk add
        --quiet
        --repository https://dl-cdn.alpinelinux.org/alpine/edge/testing
    nix
    nix-openrc

- command -v nix-shell

- rm -f /etc/init.d/hwdrivers
        /etc/init.d/machine-id

# disable sandboxing
- echo "sandbox = false" >> /etc/nix/nix.conf
- cat /etc/nix/nix.conf
- grep sandbox /etc/nix/nix.conf

- ${SUDO:-} rc-update add nix-daemon sysinit #!
- ${SUDO:-} rc-update --update
- touch /run/openrc/softlevel
- ${SUDO:-} rc-service nix-daemon start
- pstree --unicode
- rc-status

- nix-channel --list
- *nix-channel-nixpkgs

- ( set +o noglob ; ls /etc/profile.d/nix*.sh )
- cat /etc/profile.d/nix.sh
- set +o nounset
- . /etc/profile.d/nix.sh
- set -o nounset


.apt: &apt
- set -o errexit -o nounset -o noglob -o pipefail
- shopt -s failglob
- (. /etc/os-release ; echo $PRETTY_NAME)

- ${SUDO:-} apt-get update --quiet=2
- ${CHRONIC:-} ${SUDO:-} apt-get install
        --no-install-suggests
        --no-install-recommends
        --quiet=2
    ca-certificates
    nix
- '! ( set +o noglob ; ls /etc/profile.d/nix*.sh )'
- command -v nix-shell
- nix-shell --version
- nix-channel --list
- *nix-channel-nixpkgs
# check sandboxing
- cat /etc/nix/nix.conf
- grep sandbox /etc/nix/nix.conf

.pacman: &pacman
- set -o errexit -o nounset -o noglob -o pipefail
- shopt -s failglob
- (. /etc/os-release ; echo $PRETTY_NAME)

- ${SUDO:-} ${CHRONIC:-} pacman
        --sync
        --refresh
        --noconfirm
        --quiet
        --color always
    nix
- ( set +o noglob ; ls /etc/profile.d/nix*.sh ) || true
- command -v nix-shell
- nix-shell --version
- nix-channel --list
- *nix-channel-nixpkgs
# check sandboxing
- cat /etc/nix/nix.conf
- grep sandbox /etc/nix/nix.conf
- find /nix/store -name nixpkgs

###
# nix-env
###

nix-env-install-busybox-path-alpine:
    script:
    - busybox --help sh
    before_script:
    - *apk
    - nix-env --query
    - ls -ld --color /nix/var/nix/profiles/default || true
    - *nix-env-install-busybox-path
    extends:
    - .variables
    allow_failure: true # edge testing
    when: manual
    image: alpine:edge

nix-env-install-busybox-path-archlinux:
    script:
    - busybox --help sh
    before_script:
    - *pacman
    - *nix-env-install-busybox-path
    extends:
    - .variables
    when: manual
    image: archlinux

nix-env-install-busybox-path-debian-testing:
    script:
    - busybox --help sh
    before_script:
    - *apt
    - *nix-env-install-busybox-path
    extends:
    - .variables
    when: manual
    image: debian:testing-slim

nix-env-install-busybox-path-ubuntu-rolling:
    script:
    - busybox --help sh
    before_script:
    - *apt
    - *nix-env-install-busybox-path
    extends:
    - .variables
    when: manual
    image: ubuntu:rolling

###
# Build with docker
###

.nix-build-docker:
    script:
    # - time nix-build docker.nix -o ./result
    # - service docker status || true
    # - rc-status
    # - sh -c 'until service docker status ; do sleep 1 ; done'
    # - rc-status
    # - docker load -i ./result
    - docker load < $(nix-build docker.nix)
    - docker images
    - docker run --tty busybox-nix whoami || true
    - docker run --tty busybox-nix pwd
    - printenv CI_REGISTRY
    - docker --version

.apk-rc-service-docker-start: &apk-rc-service-docker-start
    # - rc-status
    - ${SUDO:-} apk add --quiet
        docker
    - ${SUDO:-} rc-update add docker sysinit #!
    - ${SUDO:-} rc-update --update
    - ${SUDO:-} rc-service docker start

nix-build-docker-alpine-edge:
    before_script:
    - *apk
    - *nix-path
    - *apk-rc-service-docker-start
    extends:
    - .nix-build-docker
    - .variables
    variables:
        BUILD_DISTRIBUTION: alpine-edge
    allow_failure: true # edge testing
    when: manual
    image:
        name: alpine:edge

nix-build-docker-devuan:
    before_script:
    - *apt
    - *nix-path
    # - ${CHRONIC:-} ${SUDO:-} apt-get install
    #         --no-install-suggests
    #         --no-install-recommends
    #         --quiet=2
    #     sysvinit-core
        # sysvinit-core # already on devuan
    # - whoami
    # - ls --color /run
    # - ls -l --color /run/initctl
    #^ prw------- 1 root root 0
    # - /sbin/init 2
    # - telinit 2
    #     openrc
    # - ${SUDO:-} openrc --help
    # - ${SUDO:-} openrc
    # - rc-status
    - ${CHRONIC:-} ${SUDO:-} apt-get install
            --no-install-suggests
            --no-install-recommends
            --quiet=2
        docker.io
# May depend on a systemd library on Debian and Ubuntu!
# Even Devuan!
    - ls --color /etc/init.d
    - /etc/init.d/docker --help || true
    - /etc/init.d/docker status || true
    - service docker status || true
    # - /etc/init.d/docker start
    - service docker start
    # - /etc/init.d/docker status
    - service docker status || true
    # - ${SUDO:-} rc-update add docker sysinit
    # - ${SUDO:-} rc-update --update
    # - ${SUDO:-} rc-service docker start
    # - ${SUDO:-} sh -c 'until docker run --volume /var/run/docker.sock:/var/run/docker.sock hello-world ; do sleep 1 ; done'
    extends:
    - .nix-build-docker
    - .variables
    variables:
        BUILD_DISTRIBUTION: devuan
    allow_failure: true # Bug
    when: manual
    image:
        name: dyne/devuan:ceres # Devuan unstable
        # chimaera Based on Debian Bullseye (11.1) 2021

###
# Build with skopeo
###

.nix-build-docker-stream: &nix-build-docker-stream
- docker --version
- $(nix-build docker-stream.nix) | docker load
- docker images
- docker run --tty busybox-nix-stream whoami || true
- docker run --tty busybox-nix-stream pwd

.nix-build-skopeo: &nix-build-skopeo
- printenv CI_REGISTRY
- echo $CI_JOB_TOKEN | ${SUDO:-} skopeo login
        --password-stdin
        --username gitlab-ci-token
    $CI_REGISTRY

- echo ${STORAGE_DRIVER:-}
- ${SUDO:-} find / -name storage.conf
- ${SUDO:-} skopeo --version
-   $(nix-build docker-stream.nix)
    | gzip --fast
    | skopeo copy
        docker-archive:/dev/stdin
        docker://${CI_REGISTRY_IMAGE}/${BUILD_DISTRIBUTION}/docker-stream
# skopeo copy docker://quay.io/skopeo/stable:latest docker://registry.example.com/skopeo:latest

nix-build-skopeo-nixos:
    script:
    - *nix-build-skopeo
    before_script:
    - set -o errexit -o nounset -o noglob -o pipefail
    - shopt -s failglob || true
    - (. /etc/os-release ; echo $PRETTY_NAME) || true
    - command -v nix-shell
    extends:
    - .variables
    variables:
        BUILD_DISTRIBUTION: nixos
    allow_failure: true # skopeo packaging bug
    when: manual
    image:
        entrypoint:
        - sh
        - -o
        - xtrace
        - -c
        -   nix-channel --list ;
            nix-channel --remove nixpkgs ;
            nix-channel
                --add $NIXOS_OLDSTABLE_URL
                    nixpkgs ;
            nix-channel --update ;
            nix-shell
                --run sh
                --packages skopeo 
        name: nixos/nix:$NIX_STABLE

nix-build-skopeo-alpine:
    script:
    # - *nix-build-docker-stream
    - *nix-build-skopeo
    before_script:
    - *apk
    - *nix-path
    - *apk-rc-service-docker-start
    - ${SUDO:-} apk add
            --update-cache
            --quiet
        containers-common
        skopeo
    extends:
    - .variables
    variables:
        BUILD_DISTRIBUTION: alpine
    allow_failure: true # edge testing
    when: manual
    image:
        name: alpine:edge

nix-build-skopeo-debian-testing:
    script:
    - *nix-build-skopeo
    before_script:
    - *apt
    - ${CHRONIC:-} ${SUDO:-} apt-get install
            --no-install-suggests
            --no-install-recommends
            --quiet=2
        skopeo
    - *nix-path
    extends:
    - .variables
    variables:
        BUILD_DISTRIBUTION: debian
    allow_failure: true # Bug
    when: manual
    image:
        name: debian:testing

nix-build-skopeo-archlinux:
    script:
    - *nix-build-skopeo
    before_script:
    - *pacman
    - ${SUDO:-} ${CHRONIC:-} pacman
            --sync
            --refresh
            --noconfirm
            --quiet
            --color always
        skopeo
    extends:
    - .variables
    variables:
        BUILD_DISTRIBUTION: archlinux
    allow_failure: true # Bug
    when: manual
    image:
        name: archlinux

image-test-alpine:
    script:
    - exit
    before_script:
    - set -o errexit -o nounset -o noglob -o pipefail
    - shopt -s failglob || true
    - (. /etc/os-release ; echo $PRETTY_NAME) || true
    when: manual
    image:
        name: registry.gitlab.com/software-packages-demo/nix/alpine/docker-stream

image-test-archlinux:
    script:
    - exit
    before_script:
    - set -o errexit -o nounset -o noglob -o pipefail
    - shopt -s failglob || true
    - (. /etc/os-release ; echo $PRETTY_NAME) || true
    when: manual
    image:
        name: registry.gitlab.com/software-packages-demo/nix/archlinux/docker-stream
