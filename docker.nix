{ pkgs ? import <nixpkgs> { system = "x86_64-linux";}
}:
pkgs.dockerTools.buildLayeredImage {
    name = "busybox-nix";
    tag = "latest";
    created = "now";
    contents = [
        pkgs.busybox
    ];
    config = {};
}
